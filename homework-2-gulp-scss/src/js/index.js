'use strict';
((D, B, W, l = arg => console.log(arg)) => {
    //D = document, B = document.body, W = window, l = console.log()

 const menuList = D.querySelector('.dropdown-menu-list');

D.querySelector('.menu-burger').addEventListener('click',(e) =>{
    e.target.classList.toggle('menu-burger__close')
    e.target.classList.toggle('menu-burger__open')
    menuList.classList.toggle('dropdown-menu-list__close')
    menuList.classList.toggle('dropdown-menu-list__open')
})

    D.addEventListener('DOMContentLoaded',() => W.dispatchEvent(new Event('resize')))

    W.addEventListener('resize', () => {

        let size = B.offsetWidth + 17;
        const btn = D.querySelector('[type=button]'),
            wrap = D.querySelector('.menu');
        //l(size)
        if( size >= 768 ){
            btn.classList.add('menu-burger__none')
            menuList.classList.add('menu-fullscreen')
            wrap.classList.remove('dropdown-menu__wrapper')
            menuList.classList.toggle('dropdown-menu-list__open', false)
            menuList.classList.toggle('dropdown-menu-list__close', true)
        } else {
            btn.classList.remove('menu-burger__none')
            wrap.classList.add('dropdown-menu__wrapper')
            menuList.classList.remove('menu-fullscreen')
        }

    })

})(document, document.body, window);